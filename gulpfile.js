// Include gulp
var gulp = require('gulp');

// Include Our Plugins
const jeditor = require('gulp-json-editor');
const exec = require('child_process').exec;


// Task to publish
gulp.task('publish', ['npm-publish']);

gulp.task('update-version', function (cb) {
    return gulp.src('package.json')
        .pipe(jeditor(function (json) {
            var nums = json.version.split('.').map(n => +n);
            nums[nums.length - 1]++;
            json.version = nums.join('.');
            return json;
        }))
        .pipe(gulp.dest('./'));
});


gulp.task('npm-publish', ['update-version'], function (cb) {
    gulp.src('package.json')
        .pipe(jeditor(function (json) {
            exec(`npm build && npm publish`, function (err, stdout, stderr) {
                console.log(stdout);
                console.log(stderr);
                cb(stderr);
            });
            return json;
        }));
});